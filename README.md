# images

A web app created using VueJS that integrates with the Imgur API to autheticate a user with OAuth2. Also allows user to upload images, as well as to display their current library of images.
https://www.udemy.com/course/vue-js-course/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
