import qs from 'qs';
import axios from 'axios';

const CLIENT_ID = '5fa1359ee70c2d8';
const ROOT_URL = 'https://api.imgur.com';
// const CLIENT_SECRET = '86d8113448c05dec0449f4f41dd46a3b06835e5d';

export default {
  login() {
    const queryString = {
      client_id: CLIENT_ID,
      response_type: 'token'
    };

    window.location = `${ROOT_URL}/oauth2/authorize?${qs.stringify(queryString)}`;
  },
  fetchImages(token) {
    return axios.get(`${ROOT_URL}/3/account/me/images`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  uploadImages(images) {
    const promises = Array.from(images).map(image => {
      const formData = new FormData();
      formData.append('image', image);

      return axios.post(`${ROOT_URL}/3/upload`, formData, {
        header: {
          Authorization: `Client-ID ${CLIENT_ID}`
        }
      });
    });

    return Promise.all(promises);
  }
};